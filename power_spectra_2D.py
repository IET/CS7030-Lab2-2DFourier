import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm

#Good Reference: http://matplotlib.org/users/pyplot_tutorial.html

def rect(t):
	if abs(t) <= 0.5:
		return 0.5
	elif abs(t) > 0.5:
		return 0

def rect2D(x,y):
	return rect(x)*rect(y)

def sinc(t):
	return np.sin(np.pi*t)/(np.pi*t)

#sinc is not easily seperable into a product of 1D components	
def sinc2D(x,y):
 	return (np.sin(np.pi*x)*np.sin(np.pi*y))/(np.pi**2*x*y)
	
def sgn(t):
	if t > 0:
		return 1
	if t == 0:
		return 0
	if t < 0:
		return -1

def sgn2D(x,y):
	return sgn(x)*sgn(y)

def tri(t):
	if abs(t) <= 1:
		return 1 - abs(t)
	else:
		return 0

def tri2D(x,y):
	return tri(x)*tri(y)
	
def imp(t):
	if t == 0:
		return 1
	else:
		return 0

def imp2D(x,y):
	return imp(x)*imp(y)

def comb(t):
	#Period of Dirac Delta. Probably shouldn'y hard-code this
	T = 0.5
	
	if t%T == 0:
		return 1
	else:
		return 0

def comb2D(x,y):
	return comb(x)*comb(y)
	
def calc_power_spectra(signal, title):
	#My attempt at 3D plots, must come back to this
	#fig, axarr = plt.subplots(2,2)
	#axarr[0,0] = fig.gca(projection='3d')

	#X, Y = np.meshgrid(X, Y)

	#axarr[0,0].plot_surface(X, Y, Z)
	
	#plt.show()
	fig, axarr = plt.subplots(2,2)
	fig.suptitle(title, fontsize=20)

	axarr[0,0].set_title("Spatial Domain")
	axarr[0,0].imshow(signal, cmap=cm.Greys_r)
	
	fft_output = np.fft.fft2(signal)
	
	axarr[1,0].set_title("Frequency Domain")
	axarr[1,0].imshow(np.log10(np.real(fft_output)), cmap=cm.Greys_r)
	
	inverse_transform = np.fft.ifft2(fft_output)
	axarr[0,1].set_title("Approximated Waveform")
	axarr[0,1].imshow(np.real(inverse_transform), cmap=cm.Greys_r)
	
	pow_spec = np.abs(fft_output)**2
	axarr[1,1].set_title("Power Spectrum")
	axarr[1,1].imshow(np.log10(pow_spec), cmap=cm.Greys_r)

	plt.savefig("{0} power spectrum.png".format(title), dpi=96)
	plt.show()



#TODO - Delete the below
def junk_code():	
	fig, axarr = plt.subplots(2,2)
	fig.subplots_adjust(hspace = 0.4)
	plt.ylim(-2, 2)
	
	axarr[0,0].set_title("Spatial Domain")
	axarr[0,0].set_xlabel("X")
	axarr[0,0].set_ylabel("Y")
	axarr[0,0].set_ylim(min(signal)-0.1,max(signal) + 0.1)
	axarr[0,0].plot(t, signal)
	
	rfft_output = np.fft.rfft(signal)

	magnitude_only = [np.sqrt(i.real**2 + i.imag**2)/len(rfft_output) for i in rfft_output]
	frequencies = freq = np.fft.fftfreq(len(signal), num_samples)
	
	axarr[1,0].set_title("Frequency Domain")
	axarr[1,0].set_xlabel("Frequency (in Hz)")
	axarr[1,0].set_ylabel("Amplitude")
	axarr[1,0].plot(rfft_output, 'r')
	
	inverse_transform = np.fft.ifft(rfft_output)
	
	axarr[0,1].set_title("Approximated Waveform")
	axarr[0,1].set_xlabel("Time")
	axarr[0,1].set_ylabel("Amplitude")
	axarr[0,1].plot(inverse_transform, 'r')
	
	pow_spec = np.array([abs(i,j)**2 for i in rfft_output])
	
	axarr[1,1].set_title("Power Spectrum")
	axarr[1,1].set_xlabel("|G(Ft)|^2")
	axarr[1,1].set_ylim(min(pow_spec)-0.1,max(pow_spec) + 0.1)
	axarr[1,1].plot(pow_spec, 'r')

	plt.savefig("{0} power spectrum.png".format(title), dpi=96)
	plt.show()

freq = 5 #hz - cycles per second
amplitude = 1
starting_point = -1.5
time_to_plot = 3 # second
sample_rate = 100 # samples per second
num_samples = sample_rate * time_to_plot

while True:
	x = np.linspace(starting_point, starting_point + time_to_plot, (3)/0.01+1)
	y = np.linspace(starting_point, starting_point + time_to_plot, (3)/0.01+1)

	index = input("Please Choose From the Following Options:\
					\n1. Rectangle\
					\n2. Sinc\
					\n3. Signum\
					\n4. Triangle\
					\n5. Impulse/Dirac Delta\
					\n6. Comb \
					\n7. Exit \n\n")
	
	function = rect2D
	title = ""
	
	if index == '1':
		title ="Rectangle Function"
		function = rect2D
	elif index == '2':
		x = y = np.linspace(starting_point, starting_point + time_to_plot, num_samples)
		function = sinc2D
		title = "Sinc Function"
	elif index == '3':
		function = sgn2D
		title = "Signum Function"
	elif index == '4':
		function = tri2D
		title = "Triangle Function"
	elif index == '5':
		function = imp2D
		title = "Impulse Function"
	elif index == '6':
		function = comb2D
		title = "Comb Function"
	elif index == '7':
		break
	else:
		print("Invalid Input, Please Choose From the available Options\n")
		break

		
	#TODO - Find a more 'pythonic' way to do this
	#Can probably be a list comprehension somehow
	z = []
	for i,k in enumerate(x):
		z.append([])
		for j in y:
			z[i].append(function(k, j))

	calc_power_spectra(z, title)
	